<?php


require_once("Entity_Interface.php");
require_once("Mysql.php");

abstract class Entity implements Entity_Interface
{
    protected static $_tableName = NULL;

    protected $reflectionClass;

    public function __construct()
    {
        try {
            $this->reflectionClass = new \ReflectionClass($this);
        } catch (ReflectionException $e) {
            var_dump($e->getMessage());
        }
    }

    /**
     * Persist an object in database (based on it's public properties)
     */
    function save(){

            //Parcours des public properties
            $properties = $this->reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC);
            foreach($properties as $property){
                $propertyName = $property->getName();

                if($propertyName !== 'id' && $propertyName != '_tableName' ){
                    $propertyValue = $property->getValue($this); // $this->>{$propertyName}
                    $props[] = '`'.$propertyName.'` = "'. $propertyValue . '"';
                }
            }

            //On tente de load l'objet via l'id
            $id = $this->reflectionClass->getProperty('id')->getValue($this);
            $object=false;
            if($id){$object = $this->load($id);}

            // Si l'objet existe, on le met à jour
            if($object){
                $sqlQuery = "UPDATE ".$this->getTableName()." SET ". implode(" , ", $props) . " WHERE id = ".$id;
            }
            // Sinon on le crée
            else{
                $sqlQuery = "INSERT INTO ".$this->getTableName()." SET ". implode(" , ", $props);
            }
            Mysql::getInstance()->getConnection()->query($sqlQuery);
            return $this;
    }

    /**
     * Load an object from id
     *
     * @param $id
     * @return mixed
     * @throws Exception
     */
    function load($id){
        $sqlQuery = "SELECT * FROM ".$this->getTableName()." WHERE id = ".$id;
        $queryResult = Mysql::getInstance()->getConnection()->query($sqlQuery)->fetchAll(PDO::FETCH_ASSOC);
        if(!$queryResult){throw new Exception(("Loaded failed"));}

        $object = self::morph($queryResult, true);
        return $object;
    }

    /**
     * Find a collection of object from a predicat
     *
     * @param $predicat
     * @return array
     * @throws ReflectionException
     */
    public static function find($predicat):array{
        $sqlQuery    = "SELECT * FROM ".self::getTableName()." WHERE ".$predicat;
        $queryResult = Mysql::getInstance()->getConnection()->query($sqlQuery);
        return $queryResult ? self::morph($queryResult->fetchAll(PDO::FETCH_ASSOC)) : [] ;
    }

    /**
     * SQL object to collection / simple object
     *
     * @param $queryResult
     * @param bool $object : convert collection to object if only 1 result
     * @return array|mixed
     * @throws ReflectionException
     */
    public static function morph($queryResult, $object=false){
        $reflectionClass = new ReflectionClass(get_called_class());

        $properties = $reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC);
        $collection = array();

        foreach($queryResult as $row){
            $object = new $reflectionClass->name();
            foreach($properties as $property){
                $propertyName = $property->getName();
                if($propertyName !== '_tableName'){
                    $property->setValue($object, $row["$propertyName"]); // $this->>{$propertyName}
                }
            }
            array_push($collection, $object);
        }

        if(sizeof($collection) == 1 && $object){
            return $collection[0];
        }
        return $collection;
    }


    public function remove()
    {
        //On récupère l'id de l'object
        $id = $this->reflectionClass->getProperty('id')->getValue($this);

        if($id){
            $sqlQuery = "DELETE FROM ".$this->getTableName()." WHERE id = ".$id;
            Mysql::getInstance()->getConnection()->query($sqlQuery);
        }
    }

    /**
     * Return the table name of Entity
     *
     * @return mixed|string
     * @throws ReflectionException
     */
    public function getTableName(){
        $reflectionClass = new ReflectionClass(get_called_class());
        $table_name = strtolower($reflectionClass->getName());
        $table_name_attribute = $reflectionClass->getProperty("_tableName")->getValue();
        if($table_name_attribute){
            $table_name = $table_name_attribute;
        }
        return $table_name;
    }
}