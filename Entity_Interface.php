<?php

interface Entity_Interface
{
    public function         save();
    public function         load($id);
    public static function  find($predicat): array;
    public function         remove();
}