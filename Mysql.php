<?php

class Mysql{

    const DEFAULT_USER = "root";
    const DEFAULT_PASS = "toor";
    const DEFAULT_HOST = "localhost";
    const DEFAULT_DBNAME = "orm";
    private $PDOInstance = NULL;
    private static $MysqlInstance = NULL;

    public function __construct()
    {
        $this->PDOInstance =  new PDO('mysql:host='.self::DEFAULT_HOST.';dbname='.self::DEFAULT_DBNAME.';charset=utf8',
            self::DEFAULT_USER ,
            self::DEFAULT_PASS);
    }



    /**
     * Return Mysql Instance from a static context (singleton)
     *
     * @return Mysql|null
     */
    public static function getInstance(){
        if(is_null(self::$MysqlInstance)){
            self::$MysqlInstance = new Mysql();
        }
        return self::$MysqlInstance;
    }

    /**
     * Return Pdo instance
     *
     * @return null|PDO
     */
    public function getConnection(){
        return $this->PDOInstance;
    }

    /**
     * @param $query
     * @return bool|PDOStatement
     */
    public function query($query){
        return $this->PDOInstance->query($query);
    }

}