<?php
require_once("Post.php");

$post          = new Post();
$post->content = "I'm a new sdqdst :)";
$post->id = 1;
$post->save();

$post          = new Post();
$post->content = "I'm a SPECIAL post";
$post->save();

$post          = new Post();
$post->content = "I'm also a SPECIAL post";
$post->save();

$post          = new Post();
$post->content = "I'm the 4th post !";
$post->save();

$post          = new Post();
$post->content = "I'm the 6th post !";
$post->save();

// Loading Object Post with id 1
var_dump($post->load(1));

// Will return a collection Post objects
var_dump(Post::find("content LIKE '%SPECIAL%'"));


// Change content of Post id 4
$post          = new Post();
$post->id      = 4;
$post->content = "I'm the 5th post !";
$post->save();


// Remove Post id 5
// /!\ Working but loading cannot be done on deleted id.
//$post          = new Post();
//$post->load(5)->remove();
