# ORM

A simple ORM made with love & PHP


## Usage

### Mysql Configuration 

Create a database "orm".

Then, the connection can be edited in the file 'Mysql.php'
```php
const DEFAULT_USER = "root";
const DEFAULT_PASS = "";
const DEFAULT_HOST = "localhost";
const DEFAULT_DBNAME = "orm";
```

### Migrations

Each table version is saved in a table named 'versions' which is updated after each migration. 

Launch a migration :

```php
cd migration/
php updateSchema.php
```

Folder containing migration configuration files :

```python
# Table creation config files : 
config/*-install.json
example : config/new_table-install.json

# Table update config files : 
config/*-update-*.json
example : config/table_to_update-update-011.json
```

#### Creations


Demos table creation config can be find in folder config/

The json config file needs several attributes to works : 

```php
"version" # The table version (010)
"name" # The name of the new table  
"fields" # The fields / columns of the table
```

Each field of table also needs mandatory attributes :  
```php
"name" # The column name
"type" # The column type (varchar(255) / int .. )  
"property" # The column property (NOT  NULL / PRIMARY KEY .. )
```


#### Updates


Update a table column by adding an 'action' attribute with an available operations:

```php
create - edit - remove - rename
```

```php
"create-a-field" : {
  "name" : "new_field",
  "type" : "varchar(255)",
  "property" : "",
  "action" : "create"
}
```

```php
"update-a-field" : {
  "name" : "value",
  "type" : "varchar(254)",
  "property" : "",
  "action" : "edit"
}
```

```php
"remove-a-field" : {
  "name" : "field_to_delete",
  "type" : "int(11)",
  "property" : "",
  "action" : "remove",
}
```

```php
# The 'rename' action need the 'target' column name
"rename-a-field" : {
  "name" : "renamed_field",
  "type" : "int(11)",
  "property" : "",
  "action" : "rename",
  "target": "field_to_rename"
}
```





### Handle Entities Objects

Launch the demo script :

```php
php run.php
```

```php
# Create an object (extending from Entity)
$post = new Post();
$post->content = "I'm a new post :)";
$post->save();
```

```php
# Update an existing object
$post = new Post();
$post->content = "I'm an existing post :)";
$post->id = 1;
$post->save();
```

```php
# Load an object
$post->load(1);
```

```php
# Find a collection of objects from a predicat 
Post::find("content LIKE '%SPECIAL%'")
```

```php
# Delete an object   
$post = new Post();
$post->load(5)->remove();

```

## Contributors
Marvin HERTSOEN

## License
[MIT](https://choosealicense.com/licenses/mit/)