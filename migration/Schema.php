<?php

require_once("../Mysql.php");
require_once("Table.php");

class Schema{
    private $install_tables = array();
    private $update_tables = array();
    private $_available_actions = ['create','remove','edit', 'rename'];

    /**
     * Schema constructor.
     */
    public function __construct()
    {
        $this->loadInstallTables();
        $this->loadUpdateTables();
        //var_dump($this->install_tables);
        //var_dump($this->update_tables);
        $this->verify();
    }

    /**
     * Load install tables config from config/*-install.json
     */
    public function loadInstallTables(){
        foreach (glob("../config/*-install.json") as $table_path){
            $table = new Table($table_path);

            // table 'versions' must be created first
            if(!$table->content['name'] === 'versions'){
                array_push($this->install_tables, $table);
            }
            else{
                array_unshift($this->install_tables, $table);
            }
        }
    }

    /**
     * Load update tables config from config/*-update.json
     */
    public function loadUpdateTables(){
        foreach (array_reverse(glob("../config/*-update-*.json")) as $table_path){
            $table = new Table($table_path);

            // table 'versions' must be created first
            if(!$table->content['name'] === 'versions'){
                array_push($this->update_tables, $table);
            }
            else{
                array_unshift($this->update_tables, $table);
            }
        }
    }



    /**
     * Update all the tables
     *
     */
    public function apply()
    {

        // Some tables must be created
        foreach ($this->install_tables as $table) {
            if ($table->isNew()) {
                $table->createTable();
                echo("Table '" . $table->content['name'] . "' created. \n");
            }
        }

        // Some tables must be updated
        foreach ($this->update_tables as $table) {
            if (!$table->isUpToDate()) {
                $this->updateTable($table);
                echo("Table '" . $table->content['name'] . "' updated to version '". $table->content['version'] ."'. \n");
            }
        }
        echo "Done. \n";
    }

    /**
     * Create all tables
     */
    public function create(){
        foreach($this->install_tables as $table){
            $table = $table->content;
            $table->deleteTable();

            $props = array();
            foreach($table->fields as $field){
                array_push($props,$field->name ." ". $field->type . " " . $field->property);
            }

            $sqlQuery = "CREATE TABLE `".$table->name."` (" . implode(" , ", $props) . ")";
            Mysql::getInstance()->getConnection()->query($sqlQuery);
        }
    }



    /**
     * Update a db table from table object
     *
     * @param $table
     */
    public function updateTable($table){
        $table_content = $table->content;
        foreach($table_content['fields'] as $field){
            if(array_key_exists('action', $field)){
                switch($field['action']){
                    case 'create' :
                        $table->createField($field);
                        break;
                    case 'remove':
                        $table->removeField($field);
                        break;
                    case 'edit':
                        $table->editField($field);
                        break;
                    case 'rename':
                        $table->renameField($field);
                        break;
                    default:
                        break;
                }
            }
        }
        $sqlQuery = "UPDATE `versions` SET value = '" . $table_content['version'] . "' WHERE table_name = '".$table_content['name']."'";
        Mysql::getInstance()->getConnection()->query($sqlQuery);
    }





    /**
     * Check the integrity of json table files
     *
     * @return bool
     * @throws Exception
     *
     */
    public function verify(){
        $mandatory_params = ['name', 'type', 'property'];

        // Each table
        foreach( $this->install_tables as $table){
            $table_content = $table->content;

            // Each field of table
            foreach($table_content['fields'] as $field){

                // Each mandatory json parameter
                foreach($mandatory_params as $mandatory_param){
                    if(!isset($field[$mandatory_param])){
                        throw new Exception("Missing mandatory parameter '". $mandatory_param ."' in table '". $table_content['name'] ."'");die;
                    }
                }

                // Specifics mandatory json parameter
                if(isset($field['action'])){

                    // action exists in the list ?
                    if(!in_array($field['action'],$this->_available_actions)){
                        throw new Exception("Incorrect action '".$field['action']."' in field '". $field['name'] ."' belonging to the table '". $table_content['name'] ."'");die;
                    }

                    // mandatory parameters exists for actions ?
                    switch($field['action']){
                        case 'rename' :
                            if(!isset($field['target'])){
                                throw new Exception("Missing mandatory parameter 'target' in field '". $field['name']."' belonging to the table '". $table_content['name'] ."'");die;
                            }
                            break;
                    }
                }
            }
        }
        return true;
    }
}




