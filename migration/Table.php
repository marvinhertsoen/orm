<?php

require_once("../Mysql.php");


class Table{
    public $content;
    public $db_version = NULL;

    /**
     * Table constructor.
     * @param null $path
     */
    public function __construct($path = NULL)
    {
        if($path){ $this->load($path); }
    }

    /**
     * Load table content from json
     *
     * @param $path
     * @return $this
     */
    function load($path){
        $this->content = json_decode(file_get_contents($path), true);
        $this->loadTableVersion();
        return $this;
    }

    /**
     * Reset table content
     *
     * @return $this
     */
    function reset(){
        $this->content = [];
        return $this;
    }

    /**
     * Check if the table is already created or not
     *
     * @return bool
     */
    function isNew(){
        if($this->db_version){return false;}
        return true;
    }

    /**
     * Check if the table is up to date
     *
     * @return bool
     */
    function isUpToDate(){
        $content  = $this->content;

        if($this->db_version < $content['version']){
            return false;
        }
        return true;
    }

    /**
     * Get the table version from database
     *
     */
    function loadTableVersion(){
        $sqlQuery = "SELECT value FROM `versions` WHERE table_name = '" . $this->content['name'] . "'";
        $result = Mysql::getInstance()->getConnection()->query($sqlQuery);

        if($result && $result_fetched = $result->fetch(PDO::FETCH_ASSOC)){
            $this->db_version = $result_fetched["value"];
        }
    }


    /**
     * Create a db table from table object
     *
     */
    public function createTable(){
        $props = array();
        foreach($this->content['fields'] as $field){
            array_push($props,$field['name'] ." ". $field['type'] . " " . $field['property']);
        }

        //Create Table
        $sqlQuery = "CREATE TABLE `".$this->content['name']."` (" . implode(" , ", $props) . ")";
        //var_dump($sqlQuery);
        Mysql::getInstance()->getConnection()->query($sqlQuery);

        //Add table version
        $sqlQuery = "INSERT INTO versions(table_name, value) VALUES('". $this->content['name'] ."', '". $this->content['version']."') ";
        //var_dump($sqlQuery);
        Mysql::getInstance()->getConnection()->query($sqlQuery);
    }


    /**
     * Drop a table from it's name
     *
     */
    public function deleteTable(){
        $sqlQuery    = "DROP TABLE `".$this->content['name'] . "`";
        Mysql::getInstance()->getConnection()->query($sqlQuery);
    }


    /**
     * Create new field DB table
     *
     * @param $table_name
     * @param $field
     */
    public function createField($field){
        $sqlQuery    = "ALTER TABLE `". $this->content['name'] . "` ADD `" . $field['name'] . "` " . $field['type'] . " " . $field['property'] ;
        Mysql::getInstance()->getConnection()->query($sqlQuery);
    }

    /**
     * remove field DB table
     *
     * @param $table_name
     * @param $field
     */
    public function removeField($field){
        $sqlQuery    = "ALTER TABLE `". $this->content['name'] . "` DROP `" . $field['name'] ."`";
        //var_dump($sqlQuery);
        Mysql::getInstance()->getConnection()->query($sqlQuery);
    }

    /**
     * Edit the field properties
     *
     * @param $field
     */
    public function editField($field){
        $sqlQuery    = "ALTER TABLE `". $this->content['name'] . "` MODIFY COLUMN `" . $field['name'] . "` " . $field['type'] . " " . $field['property'];
        //var_dump($sqlQuery);
        Mysql::getInstance()->getConnection()->query($sqlQuery);
    }

    /**
     * Rename a field
     *
     * @param $field
     */
    public function renameField($field){
        $sqlQuery    = "ALTER TABLE `". $this->content['name'] . "` DROP `" . $field['target'] ."`";
        Mysql::getInstance()->getConnection()->query($sqlQuery);

        $sqlQuery    = "ALTER TABLE `". $this->content['name'] . "` ADD `" . $field['name'] . "` " . $field['type'] . " " . $field['property'] ;
        Mysql::getInstance()->getConnection()->query($sqlQuery);
    }
}